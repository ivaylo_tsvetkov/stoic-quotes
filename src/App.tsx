import React from "react";
import "./App.css";
import { RootState } from "./redux/store";
import { useSelector, useDispatch } from "react-redux";
import { sagaActions } from "./redux/redux-saga/sagaActons";
import { QuoteCard } from "./components";

function App() {
  const content = useSelector(
    (state: RootState) => state.quotes.quoteWhitAuthor
  );
  const dispatch = useDispatch();

  const handleClick = async () => {
    dispatch({ type: sagaActions.FETCH_DATA_SAGA });
  };

  return (
    <div>
      <button onClick={handleClick}>Get random quote</button>
      {content.map((e, index) => (
        <QuoteCard author={e.author} quote={e.quote} key={index} />
      ))}
    </div>
  );
}

export default App;
