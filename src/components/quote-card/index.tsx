import React from "react";
import { Quote } from "../../redux/redux-slices/quotesSlice";

export const QuoteCard = ({ author, quote }: Quote) => {
  return (
    <div>
      <h4>{author}</h4>
      <p>{quote}</p>
    </div>
  );
};
