import { configureStore } from "@reduxjs/toolkit";
import quotesReducer from "./redux-slices/quotesSlice";
// import { composeWithDevTools } from 'redux-devtools-extension'
import createSagaMiddleware from "redux-saga";
import saga from "./redux-saga/saga";

const sagaMiddleware = createSagaMiddleware();
export const store = configureStore({
  reducer: {
    quotes: quotesReducer,
  },
  middleware: [sagaMiddleware],
});
sagaMiddleware.run(saga);

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
