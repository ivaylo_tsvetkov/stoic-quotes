import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface Quote {
  author: string;
  quote: string;
}

export interface QuotesState {
  quoteWhitAuthor: Quote[];
}

const initialState: QuotesState = {
  quoteWhitAuthor: [],
};

export const quotesSlice = createSlice({
  name: "quotes",
  initialState,
  reducers: {
    getQuote: (state, action: PayloadAction<Quote>) => {
      return {
        ...state,
        quoteWhitAuthor: [action.payload, ...state.quoteWhitAuthor],
      };
    },
  },
});

// Action creators are generated for each case reducer function
export const { getQuote } = quotesSlice.actions;

export default quotesSlice.reducer;
