import { call, takeEvery, put } from "redux-saga/effects";
import { getQuote } from "../redux-slices/quotesSlice";
import Axios, { AxiosRequestConfig } from "axios";
import { sagaActions } from "./sagaActons";

let callAPI = async ({ url, method, data }: AxiosRequestConfig) => {
  return await Axios({
    url,
    method,
    data,
  });
};

export function* fetchDataSaga(): any {
  try {
    let result = yield call(() => callAPI({ url: "/stoic-quote" }));
    yield put(getQuote(result.data.data));
  } catch (e) {
    yield put({ type: "TODO_FETCH_FAILED" });
  }
}

export default function* rootSaga() {
  yield takeEvery(sagaActions.FETCH_DATA_SAGA, fetchDataSaga);
}
